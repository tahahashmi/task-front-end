import { Component, OnInit } from "@angular/core";
import { Article } from "../Helper/article.model";
import { ArticleService } from "../Services/article.service";
import { Router } from "@angular/router";

@Component({
  selector: "app-add-new-article",
  templateUrl: "./add-new-article.component.html",
  styleUrls: ["./add-new-article.component.css"]
})
export class AddNewArticleComponent implements OnInit {
  constructor(private articleService: ArticleService, private router: Router) {}
  public article: Article = new Article();

  ngOnInit() {}
  //This method will add new Article into our DataBase 
  onSubmit() {

    this.articleService.addNewArticle(this.article).subscribe(result => {
      //if response is not null then navigate to Home Page
      if (result._id != null) {
        this.router.navigate(["/"]);
      } 
      //Else Alert Error Message
      else {
        alert("There might be some problem");
      }
    });
  }
}
