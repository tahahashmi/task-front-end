import { Component, OnInit } from '@angular/core';
import {Article} from '../Helper/article.model'
import {ArticleService} from '../Services/article.service';
import { Router } from '@angular/router';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-update-article',
  templateUrl: './update-article.component.html',
  styleUrls: ['./update-article.component.css']
})
export class UpdateArticleComponent implements OnInit {

  constructor(private articleService: ArticleService, private router: Router, public route:ActivatedRoute) {}
  public article: Article = new Article();
  public id;

  ngOnInit() {
    this.id = this.route.snapshot.params['id'];
    this.loadData();
  }
  loadData(){
    this.articleService.getParticularArticle(this.id).subscribe(result=>{
      console.log(result);
      this.article=result
    })

  }
  onSubmit(){
    this.articleService.updateArticle(this.id,this.article).subscribe(res=>{
      if (res._id != null) {
        this.router.navigate(["/"]);
      } else {
        alert("There might be some problem");
      }

    })
  }
  deleteArticle(){
    this.articleService.deleteParticularArticle(this.id).subscribe(res=>{
      if (res._id != null) {
        this.router.navigate(["/"]);
      } else {
        alert("There might be some problem");
      }

    })
  }

}
