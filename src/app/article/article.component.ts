import { Component, OnInit } from '@angular/core';
import {ArticleService} from '../Services/article.service'
import {Article} from '../Helper/article.model'
import {NgbModal, ModalDismissReasons} from '@ng-bootstrap/ng-bootstrap';
import { Router } from '@angular/router';


@Component({
  selector: 'app-article',
  templateUrl: './article.component.html',
  styleUrls: ['./article.component.css']
})
export class ArticleComponent implements OnInit {

  constructor(
    private articleService:ArticleService,
    private modalService: NgbModal
  ) { }

  // public articles:Article[]=[];
  public articles=[];
  private router: Router;
  closeResult: string;
  public userData=[];

  ngOnInit() {
    //this.loadData();
    this.articles = [
      {
      
      "title":"new Article",
      "description":"Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum",
      "image":"http://placehold.it/300x200",
      "createdAt":"2012-03-29"
    }]
    this.userData = [
    {
      'name':"Item 1",
      "inStock":'300',
    },
    {
      'name':"Item 1",
      "inStock":'300',
    },
    {
      'name':"Item 1",
      "inStock":'300',
    },
    {
      'name':"Item 1",
      "inStock":'300',
    },
    {
      'name':"Item 1",
      "inStock":'300',
    },
  ]

  }

  loadData(){
    this.articleService.getAllArticles().subscribe(result=>{
      console.log(result)
      this.articles=result;
    })
  }

 

  
    
    
  

}
