import { Injectable } from '@angular/core';
import { Http, Headers } from '@angular/http' ;
import { Observable } from 'rxjs/Observable'
import 'rxjs/add/operator/map' ;
import {Article} from '../Helper/article.model'

@Injectable({
  providedIn: 'root'
})
export class ArticleService {
  public url = 'http://localhost:3000/articles';
 
  
  


  constructor(private http:Http) { }
  getAllArticles(){
    return this.http.get(this.url).map(res=>res.json())
  }
  addNewArticle(article){
    let headers = new Headers();
    headers.append('Content-Type','application/json');
    return this.http.post(this.url,article,{headers:headers}).map(res=>res.json());

  }
  getParticularArticle(id){
    return this.http.get(this.url+"/"+id).map(res=>res.json())

  }
  deleteParticularArticle(id){
    return this.http.delete(this.url+"/"+id).map(res=>res.json())

  }
  updateArticle(id,article){
    let headers = new Headers();
    headers.append('Content-Type','application/json');
    return this.http.put(this.url+"/"+id,article,{headers:headers}).map(res=>res.json());

  }
}
