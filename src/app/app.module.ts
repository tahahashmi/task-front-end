import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import {FormsModule, ReactiveFormsModule} from '@angular/forms'
import { HttpModule } from '@angular/http';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import { RouterModule, Routes } from '@angular/router';

import { AppComponent } from './app.component';
import { ArticleComponent } from './article/article.component';
import { FooterComponent } from './footer/footer.component';
import {ArticleService} from './Services/article.service';
import { AddNewArticleComponent } from './add-new-article/add-new-article.component';
import { UpdateArticleComponent } from './update-article/update-article.component';
import { NavbarComponent } from './navbar/navbar.component'

//Here we declear all the routes in the application 
const appRoutes:Routes=[
  {path:'',component:ArticleComponent},
  {path:'add',component:AddNewArticleComponent},
  {path:'update/:id',component:UpdateArticleComponent}
]


@NgModule({
  declarations: [
    AppComponent,
    ArticleComponent,
    FooterComponent,
    AddNewArticleComponent,
    UpdateArticleComponent,
    NavbarComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    ReactiveFormsModule,
    HttpModule,
    NgbModule.forRoot(),
    RouterModule.forRoot(appRoutes)
  ],
  providers: [ArticleService],
  bootstrap: [AppComponent]
})
export class AppModule { }
